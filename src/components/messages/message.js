import React from 'react';
import './messages.css'

class Messages extends React.Component{
    constructor(props){
        super(props);
        this.chatRef = React.createRef();
    }
    render() {
        return(
        <ul ref={this.chatRef} className="chatMessages">
            {
                this.props.messages.map(
                    message=> {
                        const time = message.createdAt.split(',');
                        let messageClass;
                        if(localStorage.getItem('_ID')===message.authorId){messageClass = 'my'}else {messageClass = 'notMy'}
                        return (
                            <div className={messageClass} key={message._id}>
                                <div className='message'>
                                    <span className={messageClass==='my'?'message-author-right':'message-author-left'}> {time[1]} {message.author}</span>
                                    <br/>
                                    <span className='textMessage'>{message.text}</span>
                                </div>
                            </div>
                        )
                    }
                )
            }
        </ul>
        )
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.scrollToBottom()

    }

    scrollToBottom(){
        this.chatRef.current.scrollTop = this.chatRef.current.scrollHeight;
    }
}


export default Messages
