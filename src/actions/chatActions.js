export function setActiveChat(data) {
    return {
        type: 'SET_ACTIVE_CHAT',
        payload: data
    }
}

export function saveAllChats(data) {
    return {
        type: 'SAVE_CHAT_LIST',
        payload: data
    }
}

export function saveMyChats(data) {
    return {
        type: 'SAVE_MY_CHAT_LIST',
        payload: data
    }
}

export function addOneChat(data) {
    return{
        type: 'ADD_ONE_CHAT',
        payload: data
    }
}

export function addOneMyChat(data) {
    return{
        type: 'ADD_ONE_MY_CHAT',
        payload: data
    }
}

export function deleteOneMyChat(data) {
    return{
        type: 'DELETE_ONE_MY_CHAT',
        payload: data
    }
}

export function clearStore(data) {
    return{
        type: 'CLEAR_STORE',
        payload: data
    }
}
